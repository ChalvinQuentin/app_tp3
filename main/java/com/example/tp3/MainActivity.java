package com.example.tp3;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.support.design.widget.FloatingActionButton;
import android.widget.Toast;
import android.widget.Toolbar;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private SimpleCursorAdapter SCAdapter;
    private WeatherDbHelper WDbHelper;
    public static final int RCODE_NEW_CITY = 1;
    public static final int RCODE_UPDATE_CITY = 2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        final ListView cityListView = findViewById(R.id.cityList);
        final FloatingActionButton addActionButton = findViewById(R.id.addCityActionButton);

        WDbHelper = new WeatherDbHelper(this);
        WDbHelper.populate();

        SCAdapter = new SimpleCursorAdapter(this,
                R.layout.row,
                WDbHelper.fetchAllCities () ,
                new String[] { WeatherDbHelper.COLUMN_ICON, WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.
                COLUMN_COUNTRY, WeatherDbHelper.COLUMN_TEMPERATURE },
                new int[] { R.id.imageViewRow, R.id.cName, R.id.cCountry, R.id.temperature });
        cityListView.setAdapter(SCAdapter);

        /*final SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                @SuppressLint("StaticFieldLeak")
                AsyncTask asyncTask = new AsyncTask() {
                    @SuppressLint("WrongThread")
                    @Override
                    protected Object doInBackground(Object[] objects) {
                        try {
                            for (int i = 0 ; i < cityListView.getCount() ; ++i) {
                                City city = WDbHelper.cursorToCity((Cursor) cityListView.getItemAtPosition(i));
                                URL url = WebServiceUrl.build(city.getName(), city.getCountry());
                                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                                JSONResponseHandler jsonResponseHandler = new JSONResponseHandler(city);
                                jsonResponseHandler.readJsonStream(connection.getInputStream());
                                WDbHelper.updateCity(city);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Object o) {
                        super.onPostExecute(o);
                        SCAdapter.changeCursor(WDbHelper.fetchAllCities());
                        SCAdapter.notifyDataSetChanged();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }.execute();
            }
        });*/

        cityListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, CityActivity.class);
                intent.putExtra(City.TAG, WDbHelper.cursorToCity((Cursor) parent.getItemAtPosition(position)));
                startActivityForResult(intent, RCODE_UPDATE_CITY);
            }
        });

        cityListView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                AdapterView.AdapterContextMenuInfo del = (AdapterView.AdapterContextMenuInfo) menuInfo;
                menu.add(0, 0, 0, "Supprimer");
            }
        });

        addActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivityForResult(intent, RCODE_NEW_CITY);
            }
        });
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        AdapterView adapter = findViewById(R.id.cityList);
        Cursor target = (Cursor) adapter.getItemAtPosition(info.position);
        WeatherDbHelper dCity = new WeatherDbHelper(MainActivity.this);
        dCity.deleteCity(target);
        Intent delIntent = new Intent(MainActivity.this,MainActivity.class);
        startActivity(delIntent);
        Toast.makeText(MainActivity.this, "La ville '" + WDbHelper.cursorToCity(target).getName()  + "' à été supprimer", Toast.LENGTH_LONG).show();
        finish();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RCODE_NEW_CITY) {
            City newCity = data.getParcelableExtra("cityToAdd");
            WDbHelper.addCity(newCity);
        }
        if (resultCode == RCODE_UPDATE_CITY) {
            City upCity = data.getParcelableExtra("upCity");
            WDbHelper.updateCity(upCity);
        }

        SCAdapter.changeCursor(WDbHelper.fetchAllCities());
        SCAdapter.notifyDataSetChanged();
    }

}
