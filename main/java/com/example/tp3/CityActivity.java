package com.example.tp3;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class CityActivity extends AppCompatActivity {

    private static final String TAG = CityActivity.class.getSimpleName();
    private TextView textCityName, textCountry, textTemperature, textHumdity, textWind, textCloudiness, textLastUpdate;
    private ImageView imageWeatherCondition;
    private City city;
    public static final int RCODE_UPDATE_CITY = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        city = (City) getIntent().getParcelableExtra(City.TAG);

        textCityName = (TextView) findViewById(R.id.nameCity);
        textCountry = (TextView) findViewById(R.id.country);
        textTemperature = (TextView) findViewById(R.id.editTemperature);
        textHumdity = (TextView) findViewById(R.id.editHumidity);
        textWind = (TextView) findViewById(R.id.editWind);
        textCloudiness = (TextView) findViewById(R.id.editCloudiness);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageWeatherCondition = (ImageView) findViewById(R.id.imageView);
        updateView();
        
        final Button updateBtn = (Button) findViewById(R.id.majButton);

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("StaticFieldLeak")
                AsyncTask asyncTask = new AsyncTask() {
                    @SuppressLint("WrongThread")
                    @Override
                    protected Object doInBackground(Object[] objects) {
                        try {
                            URL url = WebServiceUrl.build(city.getName(), city.getCountry());
                            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                            JSONResponseHandler jsonResponseHandler = new JSONResponseHandler(city);
                            jsonResponseHandler.readJsonStream(connection.getInputStream());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Object o) {
                        super.onPostExecute(o);
                        updateView();
                    }
                }.execute();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CityActivity.this, MainActivity.class);
        intent.putExtra("upCity", city);
        setResult(RCODE_UPDATE_CITY, intent);
        super.onBackPressed();
    }

    private void updateView() {
        textCityName.setText(city.getName());
        textCountry.setText(city.getCountry());
        textTemperature.setText(city.getTemperature() + " °C");
        textHumdity.setText(city.getHumidity() + " %");
        textWind.setText(city.getFullWind());
        textCloudiness.setText(city.getHumidity() + " %");
        textLastUpdate.setText(city.getLastUpdate());

        if (city.getIcon() != null && !city.getIcon().isEmpty()) {
            Log.d(TAG, "icon=" + "icon_" + city.getIcon());
            imageWeatherCondition.setImageDrawable(getResources().getDrawable(getResources()
                    .getIdentifier("@drawable/" + "icon_" + city.getIcon(), null, getPackageName())));
            imageWeatherCondition.setContentDescription(city.getDescription());
        }
    }
}
