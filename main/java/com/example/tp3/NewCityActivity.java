package com.example.tp3;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;
    public static final int RCODE_NEW_CITY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        final Intent intent = getIntent();
        final City newCity = intent.getParcelableExtra("newCity");

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);

        final Button but = (Button) findViewById(R.id.majButton);

        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(String.valueOf(textName.getText()).equals("") == true || String.valueOf(textCountry.getText()).equals("") == true ) {
                    AlertDialog alertDialog = new AlertDialog.Builder(NewCityActivity.this).create();
                    alertDialog.setTitle("Ajout impossible");
                    alertDialog.setMessage("La nom de la ville et du pays doit être non vide !");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "CANCEL", new DialogInterface.OnClickListener() { public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }});
                    alertDialog.show();
                }else {
                    Intent intent = new Intent(NewCityActivity.this, MainActivity.class);
                    intent.putExtra("cityToAdd", new City(textName.getText().toString(), textCountry.getText().toString()));
                    setResult(RCODE_NEW_CITY, intent);
                    Toast.makeText(NewCityActivity.this, "La ville '" + textName.getText().toString()  + "' à été ajouté", Toast.LENGTH_LONG).show();
                    finish();
                }

            }
        });
    }


}
